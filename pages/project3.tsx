import React, { useEffect,useState } from 'react'
/* PRIME REACT */
import "primereact/resources/themes/lara-light-indigo/theme.css"
import "primereact/resources/primereact.min.css"
import "primeicons/primeicons.css"
import "primeflex/primeflex.css"
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
/* Components */
import { template2 } from '../components/template2';
import axios from 'axios';
// Material
import EditIcon from '@mui/icons-material/Edit';
import Link from 'next/link';
import Button from '@mui/material/Button';
import { useRouter } from 'next/router';
export default function () {
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    // ดึงค่าเริ่มต้นของ Projects มาแสดงในตาราง
    fetchProjects();
  }, []);

/*---------------- กด EDIT ข้อมูล ในหน้า project------------------*/
const router = useRouter()
const handleEditProject = (id:string) => {
  const config = {
    method: 'get',
    url: `http://localhost:8080/projectmember/project-members/${id}`,
  };
  axios(config)
.then(function (response) {
  console.log(JSON.stringify(response.data));
  router.push({
    pathname: '/creact1',
    query: { id: id },
  
  })
  console.log('id : ',id);
})
.catch(function (error) {
  console.log(error);
});
}

/*---------------- Fetch ข้อมูล project จาก database------------------*/
  const fetchProjects = async () => {
    const config = {
      method: 'GET',
      url: 'http://localhost:8080/project/all',
    };
    axios(config)
    .then((response:any)=>{
      console.log('fetch success : ',response.data);

      const newProjects = response.data.map(( project:any , index:any ) => {
        return ({
          ...project,
          'number':index+1,
        })
      })
      setProjects(newProjects);
    })
    .catch((error:any)=>{
      console.log('fetch error : ',error);
    })
  }
/*---------------------------------------------------------------------------*/

 
  const action1 = (rowData:any) => {
    return (
      <Link onClick={()=> {
        handleEditProject(rowData.id);
      }} 
        href='creact1'>
<Button variant="text"> <EditIcon sx={{ color: '#4C3364'}} /></Button>
  </Link>
    );
  }

  return (
    <div>
      <div className='HEAD-PROJECT3'>
        <h3>โครงการ</h3>
        </div>
        <div className='BG-PROJECT3'>
          <h3>รายชื่อโครงการ</h3>
        <div className='PROJECT3' >
        </div>
        </div>
        <div className='grid'>
        <div className='col'>

            <DataTable value={[...projects]}  paginator paginatorTemplate={template2}  first={0} rows={10}  paginatorClassName="justify-content-end" responsiveLayout="scroll" className='shadow'>
            
            <Column  header='ลำดับ' field='number' className='ui-column-data' />
            <Column  header='โครงการ' field='mainProject' className='ui-column-data' />
            <Column  header='GEN/BATCH' field='genBatch' className='ui-column-data' />
            <Column  header='วันเริ่มต้น' field='startProjectAt' className='ui-column-data' />
            <Column  header='วันสิ้นสุด' field='endProjectAt' className='ui-column-data' />
            <Column  header='จำนวนผู้เข้าร่วมทั้งหมด (คน)' field='totalEmployee' className='ui-column-data' />
            <Column  header='จำนวนผู้เข้าร่วมปัจจุบัน (คน)' field='nowEmployee' className='ui-column-data' />
            <Column  header='Ation' body={action1} className='ui-column-data' />
            
          </DataTable>
        </div>
      </div>
        </div>
  )
}
