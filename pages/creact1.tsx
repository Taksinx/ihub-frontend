import React, { useState, useEffect } from 'react';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import PersonAddAlt1Icon from '@mui/icons-material/PersonAddAlt1';
// Material
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
// import page Popup
import Button from '@mui/material/Button';
/* PRIME REACT */
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import "primereact/resources/themes/lara-light-indigo/theme.css"
import "primereact/resources/primereact.min.css"
import "primeicons/primeicons.css"
import "primeflex/primeflex.css"
/* Components */
import { template2 } from '../components/template2';
import { useRouter } from 'next/router';
import Creactpopup  from './Creactpopup';
import Delcreactpop  from './Delcreactpop';
import Addpopup from './Addpopup';
import axios from 'axios';
export default function () {  
  const [userInProject, setUserInProject] = useState([]);
  const router = useRouter()
  const { id } = router.query
  console.log('id : ',id)

  /*------------------------------------------------------*/
  const fetchEmployeeInProject = (id:any) => {
    const config = {
      method: 'get',  // get, post, patch(update บางค่า), put(update ทุกค่า), delete
      url: `http://localhost:8080/projectmember/project-members/${id}`
    };
    axios(config)
    .then((response: any) => {
      // 2. นำค่าที่ได้จากข้อ 1. มาเก็บ state users 
      console.log('fetch success:', response.data);

      const newUsersInProject = response.data.map((userInProject: any, index: number) => {
        return {
          ...userInProject,
          'number': index + 1,
          'fname_lname': `${userInProject.user.firstName} ${userInProject.user.lastName}`
        }
      });
      setUserInProject(newUsersInProject);
    })
    .catch((error:any) => {
      console.log(error);
    });
  };

  useEffect(() => {
    if (id) {
      fetchEmployeeInProject(id);
    }
  }, [id]);
   /*------------------------------------------------------*/
  return (
  
    <div>
      <div className='HEAD-PROJECT1'>
        <h3>โครงการ</h3>
        </div>
        <div className='BG-PROJECT-2'>
          <h3>ข้อมูลโครงการ</h3>
        <div className='PROJECT1' >
        
        <Box  className='BOX1'  sx={{ m:0.5, display: 'flex' ,justifyContent: 'center' }}>
          <h3>โครงการหลัก : </h3>
          <TextField  sx={{'& label': {fontFamily: 'Kanit',},'& input': {fontFamily: 'Kanit',fontWeight: 'bold',fontSize:'15'}}}  id="outlined-basic" disabled label=" โครงการหลัก "variant="filled" value={ userInProject }/>
          <h3>Gen/Batch : </h3>
          <TextField  sx={{'& label': {fontFamily: 'Kanit',},'& input': {fontFamily: 'Kanit',fontWeight: 'bold',fontSize:'15'}}}  id="outlined-basic" disabled label=" Gen/Batch "variant="filled"/>
          <h3>จำนวน (คน) :</h3>
          <TextField  sx={{'& label': {fontFamily: 'Kanit',},'& input': {fontFamily: 'Kanit',fontWeight: 'bold',fontSize:'15'}}}  id="outlined-basic" disabled label=" จำนวน (คน) "variant="filled" value={userInProject.length}/>
          </Box>
          <div className='T1'>
          <Creactpopup/>
          <Delcreactpop/>
          <Addpopup/>
          </div>
    </div>     
        </div>
        <div className='grid'>
        <div className='col'>
        <DataTable value={[...userInProject]} paginator paginatorTemplate={template2} first={0} rows={10} paginatorClassName="justify-content-end" responsiveLayout="scroll" className='shadow'>
          <Column selectionMode="multiple" headerStyle={{ width: '3rem' }} />
          <Column header='ลำดับ' field='number' className='ui-column-data' />
          <Column header='รหัสพนักงาน' field='user.userName' className='ui-column-data' />
          <Column header='ชื่อ - นามสกุล' field='fname_lname' className='ui-column-data' />
          <Column header='ตำแหน่งย่อ' field='user.position' className='ui-column-data' />
          <Column header='สังกัด' field='user.deptShort' className='ui-column-data' />
          <Column header='วันเริ่มต้น' field='' className='ui-column-data' />
          <Column header='วันสิ้นสุด' field='' className='ui-column-data' />
          <Column header='ตำแหน่งIHub' field='Positionihub.positionihub' className='ui-column-data' />
          <Column header='การปฏิบัติงาน' field='project.mainProject' className='ui-column-data' />
        </DataTable>
        </div>
      </div>
   

        </div>
  )
}