import * as React from 'react';
import { useState } from 'react';
import { useRouter } from 'next/router';
// Material
//npm install @mui/x-date-pickers
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import Autocomplete from '@mui/material/Autocomplete';
import  Box  from '@mui/material/Box';
import Stack from '@mui/material/Stack';
//npm install dayjs --save
import dayjs, { Dayjs } from 'dayjs';
import FileUpload  from './FileUpload';
import { TextField, Button } from '@mui/material';
import axios from 'axios';


export default function SelectLabels() {


  const [isDateSelected, setIsDateSelected] = React.useState(false);
  const [isDateSelected1, setIsDateSelected1] = React.useState(false);
  const router = useRouter();
  
  const [mainProject, setMainProject] = useState<string | null>(null);
  const [genBatch,setGenBatch] = useState('');

  const [projectId, setProjectId] = useState('');


  const handleSubmit = (event: any) => {
    event.preventDefault();
    if (!name1) {
      setName1Error('** กรุณาเลือกโครงการ');
    } else {
      setName1Error('');
    }
    if (!name) {
      setNameError('** กรุณากรอก GEN/BATCH');
    } else {
      setNameError('');
    }
    if (!isDateSelected) {
      setDateError('** กรุณาเลือกวันเริ่มต้น');
    } else {
      setDateError('');
    }
    if (!isDateSelected1) {
      setDateError1('** กรุณาเลือกวันสิ้นสุด');
    } else {
      setDateError1('');
    }
    if (name1 && name && isDateSelected && isDateSelected1 ) {
      console.log('Form submitted with name:', name1, name , isDateSelected,isDateSelected1 );
      router.push(`/project2?mainProject=${mainProject}&genBatch=${genBatch}`);
    }
    const projectId = 'xxxxx'; // แทนค่าโครงการหลัก
  const genBatchId = 'yyyyy'; // แทนค่า GEN/BATCH
  };
  
    const [name, setName] = useState('');
    const [nameError, setNameError] = useState('');
    const [name1, setName1] = useState('');
    const [name1Error, setName1Error] = useState('');
    const [dateError, setDateError] = useState('');
    const [dateError1, setDateError1] = useState('');


/*------------------------------ แจ้ง ERROR ของ genBatch --------------------------------*/
      const handleNameChange = (event:any) => {
      let value = event.target.value.trim();
      setName(value);
      if (!value) {
        setNameError('');
      } else {
        setNameError('');
      }
      console.log('genBatch : ',value);
    };

/*------------------------------ แจ้ง ERROR ของ mainProject --------------------------------*/
    const handleName1Change = (event: any, value: string | null) => {
      if (!value) {
        setName1Error('');
      } else {
        setName1Error('');
      }
      setName1(value || '');

      console.log('mainProject : ',value);
    };
    /*--------------------------------------------------------------*/
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
/*------------------------------ แจ้ง ERROR ของ วันเริ่มต้น --------------------------------*/
const [startProjectAt, setStartProjectAt] = useState<Dayjs>(dayjs(''));

    let handleChange = (newValue:any) => {
      setStartProjectAt(newValue);
      if (!newValue) {
        setDateError('');
      } else {
        setDateError('');
        setIsDateSelected(true);
      }
      console.log('startProjectAt : ',newValue);
    };
/*------------------------------ แจ้ง ERROR ของ วันสิ้นสุด --------------------------------*/ 
    const [endProjectAt, setEndProjectAt] = useState<Dayjs>(dayjs(''));

    const handleChange2 = (newValue: any) => {
      setEndProjectAt(newValue);
      if (!newValue) {
        setDateError1('');
      } else {
        setDateError1('');
        setIsDateSelected1(true);
      }
      console.log('endProjectAt : ',newValue);
    };
/*---------------------------------------------------------------------------------*/  
// {/* ----------------------------------------------- ยิง API ให้เพิ่มข้อมูล ----------------------------------------------- */}
//   // ฟังก์ชั่นในการยิง request ไป nestjs เพื่อเพิ่ม user ในฐานข้อมูล
  const handleAddProject = () => {
    const data = JSON.stringify({
      "mainProject": mainProject,
      "genBatch": genBatch,
      "startProjectAt": startProjectAt,
      "endProjectAt": endProjectAt,
    });

    const config = {
      method: 'post',
      url: 'http://localhost:8080/project/create',
      headers: { 
        'Content-Type': 'application/json'
      },
      data : data
    };
    axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      const projectId = response.data.id;
      localStorage.setItem('projectId', projectId);
      setProjectId(projectId);
      console.log(projectId);
    })
    .catch(function (error) {
      console.log(error);
    });
};


// const handleGetProject = () => {
//   const projectId = localStorage.getItem('projectId');
//   console.log(projectId);
// };
    
  return (
    <div>
      <div className='HEAD-PROJECT'>
        <h3>โครงการ</h3>
        </div>
        <div className='BG-PROJECT'>
          <h3>สร้างโครงการ</h3>
        <div className='PROJECT-1' >
        <h3>โครงการหลัก :</h3>
        <Autocomplete sx={{  width: 550}} 
        value={ mainProject } 
        onChange={(event, newValue) => { // console.log('onChange', newValue);
          setMainProject(newValue);
          handleName1Change(event, newValue);
        }}
        id="name" 
        options={['GEMs', 'AdHoc', 'DevPool',]}
        // onChange={handleName1Change}
        renderInput={(params) => (
          <TextField {...params} 
          sx={{'& label': {fontFamily: 'Kanit',},'& input': {fontFamily: 'Kanit',fontWeight: 'normal',fontSize:'15'}}} 
          label="โครงการหลัก"  
          variant="outlined" 
          error={!!name1Error} 
          helperText={name1Error}/> )}/>
      </div>
    

      <div className='PROJECT-2' >
      <h3>GEN/BATCH :</h3>
      <form onSubmit={handleSubmit} >
      <TextField       sx={{width: 550,'& label': {fontFamily: 'Kanit',},'& input': {fontFamily: 'Kanit',fontWeight: 'normal',fontSize:'15'}}} label="GEN/BATCH" variant="outlined" 
        value={ genBatch } 
        onChange={(e) => {
          setGenBatch(e.target.value);
          handleNameChange(e)
        }}
        error={!!nameError} helperText={nameError}/>
        </form>
      </div>
     

      <div className='PROJECT-3' >
      <h3>วันเริ่มต้น :</h3>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Stack spacing={3}>
      <DesktopDatePicker
  className='PROJECT-3-1'
  label="วัน / เดือน / ปี"
  inputFormat="DD/MM/YYYY"
  value={ startProjectAt }
  onChange={handleChange}
  renderInput={(params) => (
    <TextField
      {...params}
      sx={{'& label': {fontFamily: 'Kanit',},'& input': {fontFamily: 'Kanit',fontWeight: 'normal'}}}
      error={!!dateError}
      helperText={dateError}
    />
  )}
/>
      </Stack>
      

    <h3>วันสิ้นสุด :</h3>
      <Stack spacing={3}>
      <DesktopDatePicker
  className='PROJECT-3-2'
  label="วัน / เดือน / ปี"
  inputFormat="DD/MM/YYYY"
  value={ endProjectAt }
  onChange={handleChange2}
  renderInput={(params) => (
    <TextField
      {...params}
      sx={{
        '& label': {
          fontFamily: 'Kanit',

        },
        '& input': {
          fontFamily: 'Kanit',
          fontWeight: 'normal'
        }
      }}
      error={!!dateError}
      helperText={dateError}
    />
  )}
/>
      </Stack>
    </LocalizationProvider>
      </div>
      <div className='PROJECT-4' >
      <h3>รายละเอียด :</h3>
      <Box component="form"
      noValidate autoComplete="off">
         <TextField
          id="outlined-multiline-static"
          sx={{width: 550,'& label': {fontFamily: 'Kanit',},'& input': {fontFamily: 'Kanit',fontWeight: 'normal'}}}
          label="กรุณากรอกรายละเอียด" multiline rows={4} />
    </Box>
    </div>
          <div className='PROJECT-5'>
          <h3>อนุมัติให้จัดโครงการ :</h3>
          <FileUpload/>
          </div>

          <div className='PROJECT-6'>
          <form onSubmit={handleSubmit} >
          <Button  
            type="submit"
            onClick={ handleAddProject }
            className='bt-1'  
            sx={{backgroundColor:'#4C3364',color:'#FFFFFF',width:200,"&:hover":{backgroundColor:'#4C3364'}}}
            variant="contained" >
          <h5>บันทึก</h5>
          </Button>
    </form>
    </div>  

        </div>
    </div>
  );
}