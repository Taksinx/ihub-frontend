import React, { useState, useEffect } from 'react';
import axios from 'axios';
// Material
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
// import page Popup
import Popup  from './Popup';
/* PRIME REACT */
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import "primereact/resources/themes/lara-light-indigo/theme.css"
import "primereact/resources/primereact.min.css"
import "primeicons/primeicons.css"
import "primeflex/primeflex.css"
/* Components */
import { template2 } from '../components/template2';
import { useRouter } from 'next/router';

import Delete  from './Delete';
export default function () {  

  const handleAddEmployeeToProject = () => {
    // const ids = member_id.map((member) => member.id);
    const member_id = addEmployeeInfo;
    const ids = member_id.map((addEmployeeInfo) => addEmployeeInfo.id);
    console.log('ids is :',ids)


    const data = JSON.stringify({
      "project_id": localStorage.getItem("projectId"),
      "member_id": ids
    });
    console.log('localStorage.getItem("projectId") : ',localStorage.getItem("projectId"));
    const config = {
      method: 'post',
      url: 'http://localhost:8080/projectmember/',
      headers: { 
        'Content-Type': 'application/json'
      },
      data: data
    };
  
    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const router = useRouter()

  const { mainProject,genBatch} = router.query

  const projectId = localStorage.getItem('projectId');

  const [ addEmployeeInfo, setAddEmployeeInfo] = React.useState([{
    id:'',
    username: '',
    firstname: '',
    lastname: '',
    position: '',
    deptShort: '',
   }]);
  //  const newEmployee = addEmployeeInfo.map((addEmployeeInfo:any,item:any, index:number) => {
  //     return {
  //       ...item,
  //       'number': index + 1,
  //       'fname_lname': `${addEmployeeInfo.firstName} ${addEmployeeInfo.lastName}`
  //     }
  //  });
  //  setAddEmployeeInfo(newEmployee);








   console.log('ค่าที่มาของ addEmployeeInfo คือ : ',addEmployeeInfo)
  const action = (rowData:any) => {
    return (
            <div className='ED-POPUP'>
             <Delete/>
             </div>
    );
  }
  
  return (
  
    <div>
      <div className='HEAD-PROJECT1'>
        <h3>โครงการ</h3>
        </div>
        <div className='BG-PROJECT1'>
          <h3>ข้อมูลโครงการ</h3>
        <div className='PROJECT1' >
        
        <Box className='BOX1'  sx={{ m:0.5, display: 'flex' ,justifyContent: 'center' }}>
          <h3>โครงการหลัก : </h3>
          <TextField  sx={{'& label': {fontFamily: 'Kanit',},'& input': {fontFamily: 'Kanit',fontWeight: 'bold',fontSize:'15'}}}  id="outlined-basic" disabled label=" โครงการหลัก "variant="filled" defaultValue={ mainProject }/>
          <h3>Gen/Batch : </h3>
          <TextField  sx={{'& label': {fontFamily: 'Kanit',},'& input': {fontFamily: 'Kanit',fontWeight: 'bold',fontSize:'15'}}}  id="outlined-basic" disabled label=" Gen/Batch "variant="filled" defaultValue={ genBatch }/>
          <h3>จำนวน (คน) :</h3>
          <TextField  sx={{'& label': {fontFamily: 'Kanit',},'& input': {fontFamily: 'Kanit',fontWeight: 'bold',fontSize:'15'}}}  id="outlined-basic" disabled label=" จำนวน (คน) "variant="filled"  value={addEmployeeInfo. length}/>
          {/* value={addEmployeeInfo.data.length} */}
        </Box>

    </div>     
    <div className='BOX2'>
      <h3>Member</h3>
      <div className='POPUP'>
      <Popup
      addEmployeeInfo = { addEmployeeInfo }
      setAddEmployeeInfo = { setAddEmployeeInfo }
      />
      </div>
    </div>
        </div>
        <div className='grid'>
        <div className='col'>
          <DataTable value={[...addEmployeeInfo]} paginator paginatorTemplate={template2}  first={0} rows={10}  paginatorClassName="justify-content-end" responsiveLayout="scroll" className='shadow'>
            {/* <Column  header='ลำดับ' field='number' className='ui-column-data'  /> */}
            <Column  header='รหัสพนักงาน' field='userName' className='ui-column-data' />
            <Column  header='ชื่อ' field='firstName' className='ui-column-data' />
            <Column  header='นามสกุล' field='lastName' className='ui-column-data' />
            <Column  header='ตำแหน่งย่อ' field='position' className='ui-column-data' />
            <Column  header='สังกัด' field='deptShort' className='ui-column-data' />
            <Column  header='DELETE'body={action}></Column>
            {/* value={addEmployeeInfo.data.map((item:any, index:number) => ({ ...item, number: index + 1 }))}  */}
          </DataTable>
        </div>
      </div>
          <div style={{ display: 'flex', justifyContent: 'center' }} >
          <Button 
          className='bt-1'   
          sx={{backgroundColor:'#4C3364',color:'#FFFFFF',width:150,"&:hover":{backgroundColor:'#4C3364'}}}   
          variant="contained"
          onClick={ handleAddEmployeeToProject }>บันทึก</Button>
          <Button className='bt-1' color="secondary"  sx={{width:150, ml:1,}}  variant="outlined">ยกเลิก</Button>
          </div>
       </div>
  )
}