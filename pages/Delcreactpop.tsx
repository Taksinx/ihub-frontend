import * as React from 'react';
// Material
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DeleteIcon from '@mui/icons-material/Delete';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';

export default function AlertDialog(props:any) {
  const { handleDeleteUser, id } = props;
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>

      <Button variant="text"onClick={handleClickOpen}>
      <DeleteIcon sx={{ color: '#513252'}} />
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
        <DeleteOutlineIcon  sx={{ fontSize: 150 , color: '#4C3364',mx:16 }} />
          <DialogContentText id="alert-dialog-description"sx={{color: '#000',ml:10 }} >
    <h4>คุณแน่ใจหรือไม่ว่าคุณต้องการลบรายชื่อนี้?</h4>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
        <Button onClick={handleClose}>ยกเลิก</Button>
          <Button onClick={handleClose} autoFocus>
            บันทึก
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}