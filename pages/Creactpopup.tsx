import EditIcon from '@mui/icons-material/Edit';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import React, { useState } from "react";
import { Calendar } from 'primereact/calendar';

import { useRouter } from 'next/router';
// Material
//npm install @mui/x-date-pickers
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';

import  Box  from '@mui/material/Box';
import Stack from '@mui/material/Stack';
//npm install dayjs --save
import dayjs, { Dayjs } from 'dayjs';
import FileUpload  from './FileUpload';
export default function AlertDialog() {

  const [value2, setValue2] = React.useState<Dayjs | null>(
   
  );

  const handleChange2 = (newValue: Dayjs | null) => {
    setValue2(newValue);
  };
  const [value3, setValue3] = React.useState<Dayjs | null>(
    
  );

  const handleChange3 = (newValue: Dayjs | null) => {
    setValue3(newValue);
  };


  const [date, setDate] = useState(null);


  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [value, setValue] = useState(null);
  const [value1, setValue1] = useState(null);
  const options = ['FULLTIME', 'PARTTIME'];
  const options1 = ['GEMs', 'AdHoc', 'DevPool'];
  const handleOnChange = (event:any, newValue:any) => {
    setValue(newValue);
  };
  const handleOnChange1 = (event:any, newValue:any) => {
    setValue1(newValue);
  };
  return (
    <div>
           <Button variant="text"onClick={handleClickOpen}>
      <EditIcon sx={{ color: '#513252'}} />
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description" >
        <DialogTitle>
        <div className='PROJECT-7' >
      
        <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Stack spacing={3}>
        <DesktopDatePicker
          label="วันเริ่มต้น"
          inputFormat="MM/DD/YYYY"
          value={value2}
          onChange={handleChange2}
          renderInput={(params) => <TextField {...params} />}
        />
      </Stack>
    </LocalizationProvider>
    </div>
<div className='PROJECT-7' >
      
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Stack spacing={3}>
        <DesktopDatePicker
          label="วันสิ้นสุด"
          inputFormat="MM/DD/YYYY"
          value={value3}
          onChange={handleChange3}
          renderInput={(params) => <TextField {...params} />}
        />
      </Stack>
    </LocalizationProvider>
    </div>
    
    <div className='PROJECT-8' >
        <Autocomplete
      value={value}
      onChange={handleOnChange}
      options={options}
      renderInput={(params) => (
        <TextField
          {...params}
          sx={{ m: 1, width: 250 }}
          label="เวลาปฎิบัติงาน"/>)}/>
       
       </div>
       <div className='PROJECT-8' >
      
       <Autocomplete
      value={value1}
      onChange={handleOnChange1}
      options={options1}
      renderInput={(params) => (
        <TextField
          {...params}
          sx={{ m: 1, width: 250 }}
          label="ตำแหน่งใน IHub "/>)}/>
            
             </div>
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose}>ยกเลิก</Button>
          <Button onClick={handleClose} autoFocus>
            บันทึก
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
