import * as React  from 'react';
// Material
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

export function WorkTimeType(props:any) {
  const { addUserInfo, setAddUserInfo} = props;
  const [isInputsEmpty, setIsInputsEmpty] = React.useState(true);

  React.useEffect(() => {
    const hasEmptyInput = Object.values(addUserInfo).some(val => val === '');
    setIsInputsEmpty(hasEmptyInput);
  }, [addUserInfo]);

    return (
      <Autocomplete
        options={workTimeType}
        sx={{ m: 1,width: 400 }}
        renderInput={(params) => (<TextField {...params}  className='b-1' label="การปฏิบัติงาน" error={isInputsEmpty && addUserInfo.workTimeType === ''}helperText={isInputsEmpty && addUserInfo.performance === '' ? 'กรุณาเลือกการปฏิบัติการ' : ''}/>)}
        // value={''}
        value={addUserInfo.workTimeType}
        onChange={(event, newValue) => {
        setAddUserInfo({ ...addUserInfo, workTimeType: newValue });
      }}
      />
    );
  }
  
  const workTimeType = [
    'Full-Time',
    'Part-Time',
  ];